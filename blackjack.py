from cardgame_base import *

class Blackjack():
    def __init__(self):
        list_of_values = list(range(1, 14))
        list_of_suits = ['Spades', 'Hearts', 'Diamonds', 'Clubs']
        list_of_cards = [Card(value, suit) for value in list_of_values for suit in list_of_suits]
        self.deck = Deck(list_of_cards)
    
    def _convert_to_string(self, card: Card) -> str:
        conversion_dict = {1: 'Ace', 11: 'Jack', 12: 'Queen', 13: 'King'}
        # try:
        #     string_value = conversion_dict[card.card_value]
        #     return f"{string_value} of {card.card_suit}"
        # except KeyError:
        #     return f"{card.card_value} of {card.card_suit}"
        return f"{conversion_dict.get(card.card_value, card.card_value)} of {card.card_suit}"
    
    def _check_cards_value(self, cards) -> int:
        value = sum(card.card_value for card in cards)
        for card in cards:
            if card.card_value == 1:
                temp_value = value + 10
                if temp_value < 21:
                    value = temp_value
        return value

    def play(self):
        print("Playing Blackjack")
        print("Shuffling Deck")
        self.deck.shuffle()
        player_hand = [self.deck.draw_card(), self.deck.draw_card()]
        print(f"Your two cards are {self._convert_to_string(player_hand[0])} and {self._convert_to_string(player_hand[1])}")
        print(f"The total value of your cards is {self._check_cards_value(player_hand)}")
        if self._check_cards_value(player_hand) > 21:
            print("Oh no! You busted!")
            return
        dealer_hand = [self.deck.draw_card(), self.deck.draw_card()]
        print(f"The dealer is showing {self._convert_to_string(dealer_hand[0])} and {self._convert_to_string(dealer_hand[1])}")
        print(f"The total value the Dealers cards is {self._check_cards_value(dealer_hand)}")
        if self._check_cards_value(dealer_hand) > 21:
                print("Congrats! The Dealer busted!")
                return
        while True:
            draw = input("Would you like to draw a card? y/n  ")
            if draw != 'y' and draw != 'n':
                print("Please enter 'y' to draw or 'n' to pass")
            if draw == 'y':
                drawn_card = self.deck.draw_card()
                player_hand.append(drawn_card)
                print(f"You drew a {self._convert_to_string(drawn_card)}")
                print(f"Your hand is now {[self._convert_to_string(card) for card in player_hand]}")
                hand_value = self._check_cards_value(player_hand)
                print(f"The total value of your cards is {hand_value}")
                if hand_value > 21:
                    print("Oh no! You busted!")
                    return
            elif draw == 'n':
                break
        while self._check_cards_value(dealer_hand) < 18:
            drawn_card = self.deck.draw_card()
            dealer_hand.append(drawn_card)
            print(f"The Dealer drew a {self._convert_to_string(drawn_card)}")
            print(f"The Dealer is now showing {[self._convert_to_string(card) for card in dealer_hand]}")
            hand_value = self._check_cards_value(dealer_hand)
            print(f"The total value of the Dealers cards is {hand_value}")
            if hand_value > 21:
                print("Congrats! The Dealer busted!")
                return
        if self._check_cards_value(player_hand) > self._check_cards_value(dealer_hand):
            print("Congrats you win!")
        elif self._check_cards_value(player_hand) < self._check_cards_value(dealer_hand):
            print("Oh no you lost!")
        else:
            print("It was a tie!")

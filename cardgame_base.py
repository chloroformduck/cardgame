import random

__all__ = ['Card', 'Deck']

class Card():
    def __init__(self, card_value: int, card_suit: str) -> None:
        self.card_value = card_value
        self.card_suit = card_suit

class Deck():
    def __init__(self, starting_cards: list) -> None:
        self.starting_cards = starting_cards
        self.current_cards = starting_cards

    def shuffle(self):
        # self.current_cards = self.starting_cards
        random.shuffle(self.current_cards)

    def draw_card(self) -> Card:
        # card = random.choice(self.current_cards)
        # self.current_cards.remove(card)
        # return card
        return self.current_cards.pop()

    def return_card(self, card) -> None:
        self.current_cards.append(card)
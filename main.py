import blackjack

def main():
    game = blackjack.Blackjack()
    game.play()

if __name__ == "__main__":
    main()